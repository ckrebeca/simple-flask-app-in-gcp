from flask import Flask, url_for, request
from flask_restful import Resource, reqparse


app = Flask(__name__)


STUDENTS = {
'1': {'Student_Name': 'MARK', 'Student_Age': 23, 'Specialization': 'MATH'},
'2': {'Student_Name': 'JANE', 'Student_Age': 20, 'Specialization': 'BIOLOGY'},
'3': {'Student_Name': 'PETER', 'Student_Age': 21, 'Specialization': 'HISTORY'},
'4': {'Student_Name': 'KATE', 'Student_Age': 22, 'Specialization': 'SCIENCE'},
}


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return 'POST'
    else:
        return 'GET'

@app.route('/')
def home():
  return 'Home \n Try /students/'


@app.route('/students/', methods=['GET', 'POST'])
def student_list():
  if request.method == 'POST':
    parser = reqparse.RequestParser()
    parser.add_argument("name")
    parser.add_argument("age")
    parser.add_argument("spec")
    args = parser.parse_args()
    student_id = int(max(STUDENTS.keys())) + 1
    student_id = '%i' % student_id
    STUDENTS[student_id] = {
      "Student_Name": args["name"].upper(),
      "Student_Age": int(args["age"]),
      "Specialization": args["spec"].upper(),
    }
    return STUDENTS[student_id], 201
  else:
    return STUDENTS
  

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
