Flask==1.1.1
Flask-RESTful==0.3.8
gunicorn==20.0.4; python_version > '3.0'
gunicorn==19.10.0; python_version < '3.0'