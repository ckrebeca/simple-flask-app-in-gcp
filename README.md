**On GCP Console execute the following commands;**

(You can use the resources from this Qwiklab (*requires 5 credits*) : [Deploying a Python Flask Web Application to App Engine Flexible  ](https://www.qwiklabs.com/focuses/3339?catalog_rank=%7B%22rank%22%3A2%2C%22num_filters%22%3A0%2C%22has_search%22%3Atrue%7D&parent=catalog&search_id=5571238))


Activate console: 

`git clone https://gitlab.com/ckrebeca/simple-flask-app-in-gcp.git`

`cd simple-flask-app-in-gcp`

`virtualenv -p python3 env`

`source env/bin/activate`

`pip install -r requirements.txt`


Test app locally

`python main.py`


URL : link/students/

Example: https://8080-dot-12375928-dot-devshell.appspot.com/students/

**POSTMAN TEST**
1. Set GET to POST
2. Add keys for name, age, and spec


**DEPLOYING APP**

`gcloud app create`

*Suggested US-CENTRAL*

`gcloud app deploy`

